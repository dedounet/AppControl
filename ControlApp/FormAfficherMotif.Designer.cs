﻿namespace motif
{
    partial class FormAfficherMotif
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BpOkAffMotif = new System.Windows.Forms.Button();
            this.BpCancelAffMotif = new System.Windows.Forms.Button();
            this.listBoxMotif = new System.Windows.Forms.ListBox();
            this.checkedListBox1 = new System.Windows.Forms.CheckedListBox();
            this.BpSelectionner = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // BpOkAffMotif
            // 
            this.BpOkAffMotif.Location = new System.Drawing.Point(53, 122);
            this.BpOkAffMotif.Margin = new System.Windows.Forms.Padding(2);
            this.BpOkAffMotif.Name = "BpOkAffMotif";
            this.BpOkAffMotif.Size = new System.Drawing.Size(79, 25);
            this.BpOkAffMotif.TabIndex = 0;
            this.BpOkAffMotif.Text = "Ok";
            this.BpOkAffMotif.UseVisualStyleBackColor = true;
            // 
            // BpCancelAffMotif
            // 
            this.BpCancelAffMotif.Location = new System.Drawing.Point(161, 122);
            this.BpCancelAffMotif.Margin = new System.Windows.Forms.Padding(2);
            this.BpCancelAffMotif.Name = "BpCancelAffMotif";
            this.BpCancelAffMotif.Size = new System.Drawing.Size(79, 25);
            this.BpCancelAffMotif.TabIndex = 1;
            this.BpCancelAffMotif.Text = "Cancel";
            this.BpCancelAffMotif.UseVisualStyleBackColor = true;
            // 
            // listBoxMotif
            // 
            this.listBoxMotif.FormattingEnabled = true;
            this.listBoxMotif.Location = new System.Drawing.Point(16, 16);
            this.listBoxMotif.Margin = new System.Windows.Forms.Padding(2);
            this.listBoxMotif.MultiColumn = true;
            this.listBoxMotif.Name = "listBoxMotif";
            this.listBoxMotif.Size = new System.Drawing.Size(81, 56);
            this.listBoxMotif.TabIndex = 2;
            this.listBoxMotif.SelectedIndexChanged += new System.EventHandler(this.listBoxMotif_SelectedIndexChanged);
            // 
            // checkedListBox1
            // 
            this.checkedListBox1.FormattingEnabled = true;
            this.checkedListBox1.Location = new System.Drawing.Point(141, 16);
            this.checkedListBox1.Margin = new System.Windows.Forms.Padding(2);
            this.checkedListBox1.Name = "checkedListBox1";
            this.checkedListBox1.Size = new System.Drawing.Size(81, 49);
            this.checkedListBox1.TabIndex = 3;
            this.checkedListBox1.SelectedIndexChanged += new System.EventHandler(this.checkedListBox1_SelectedIndexChanged);
            // 
            // BpSelectionner
            // 
            this.BpSelectionner.Location = new System.Drawing.Point(91, 76);
            this.BpSelectionner.Margin = new System.Windows.Forms.Padding(2);
            this.BpSelectionner.Name = "BpSelectionner";
            this.BpSelectionner.Size = new System.Drawing.Size(79, 25);
            this.BpSelectionner.TabIndex = 4;
            this.BpSelectionner.Text = "Selectionner";
            this.BpSelectionner.UseVisualStyleBackColor = true;
            this.BpSelectionner.Click += new System.EventHandler(this.BpSelectionner_Click);
            // 
            // FormAfficherMotif
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(405, 360);
            this.Controls.Add(this.BpSelectionner);
            this.Controls.Add(this.checkedListBox1);
            this.Controls.Add(this.listBoxMotif);
            this.Controls.Add(this.BpCancelAffMotif);
            this.Controls.Add(this.BpOkAffMotif);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "FormAfficherMotif";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "AfficherMotif";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormAfficherMotif_FormClosed);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button BpOkAffMotif;
        private System.Windows.Forms.Button BpCancelAffMotif;
        private System.Windows.Forms.ListBox listBoxMotif;
        private System.Windows.Forms.CheckedListBox checkedListBox1;
        private System.Windows.Forms.Button BpSelectionner;
    }
}