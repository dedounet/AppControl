﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.IO.Ports;
//using System.Windows.Forms;

namespace testXbeeThread
{
    class XBeeThread
    {
        SerialPort myXbee = new SerialPort("COM3");

        public XBeeThread()
        {
            //myXbee = new SerialPort("COM3");
            myXbee.BaudRate = 9600;
            myXbee.Parity = Parity.None;
            myXbee.StopBits = StopBits.One;
            myXbee.DataBits = 8;
            myXbee.NewLine = "Fin"; 
            //myXbee.Handshake = Handshake.None;

            myXbee.Open();
        }

        public void Envoyer(string data)
        {
            myXbee.Write(data);
        }

        public string Recevoir()
        {
            string data = "";
            data = myXbee.ReadLine();
            return data;
        }
    }

    class tTrame
    {
        int idTrame = 6;
        String data;

        public String serialize()
        {
            String trameToSend = "";
            switch (idTrame)
            {
                case 1:
                    //Afficher le motif(id motif)
                    trameToSend = "1;" + data;
                    break;
                case 2:
                    //Afficher le pg lumineux(id pg)
                    trameToSend = "1;" + data;
                    break;
                case 3:
                    //Stopper l'exécution du programme en cours
                    trameToSend = "3";
                    break;
                case 4:
                    //Stocker programme dans la mémoire non-volatile du uC
                    trameToSend = "4;" + data;
                    break;
                case 6:
                    //Quel est le programme en cours d'exécution ?
                    trameToSend = "6";
                    break;
                case 8:
                    //Quelle est la luminosité mesurée ?
                    trameToSend = "8";
                    break;
                case 10:
                    //Test de communication
                    trameToSend = "10";
                    break;
                default:
                    Console.WriteLine("Default Case");
                    break;
            }
            trameToSend = trameToSend + ";Fin";
            Console.WriteLine("Trame envoyee : {0}", trameToSend);

            //xBee.Envoyer(trameToSend);
            /*if (trameToSend == "10")
            {
                //démarrage d'un timer pour vérifier un timeOut qui au bout de 5s sans réponse trame(id 11) affiche un pop-up
                //qui informe l'utilisateur que le test est non-concluant.
                MessageBoxButtons buttons = MessageBoxButtons.OK;
                DialogResult result;
                const string caption = "Form Closing";
                string message = "Test communication KO";
                //Display the MessageBox
                result = MessageBox.Show(message, caption, buttons);
            }*/
            return trameToSend;
        }

        public void deserialize(string trameToRead)
        {
            String paramsRetour = "";
            int idRetour;
            char[] delimeterChars = { ';' };
            string[] words = trameToRead.Split(delimeterChars);
            idRetour = Int32.Parse(words[0]);
            //paramsRetour = words[1];
            /*MessageBoxButtons buttons = MessageBoxButtons.OK;
            DialogResult result;
            const string caption = "Form Closing";*/
            Console.WriteLine("Trame reçue : {0}Fin", trameToRead);

            switch (idRetour)
            {
                case 7:
                    paramsRetour = words[1];
                    Console.WriteLine("Id de la trame reçue : {0}", idRetour);
                    Console.WriteLine("Contenu de la trame reçue : {0}",paramsRetour);
                    //Recherche du nom du programme en cours d'exécution par rapport à l'id retourné par le uC
                    //Display the MessageBox
                    //result = MessageBox.Show(paramsRetour, caption, buttons);
                    break;
                case 9:
                    paramsRetour = words[1];
                    //Affichage d'un pop-up qui affiche la valeur la valeur contenue dans paramsRetour
                    //Display the MessageBox
                    //result = MessageBox.Show(paramsRetour, caption, buttons);
                    break;
                case 11:
                    //Affichage d'un pop-up indiquant que le test de communication avec le système distant est concluant
                    //Display the MessageBox
                    //string message = "Test communication OK";
                    //result = MessageBox.Show(message, caption, buttons);
                    break;
                case 12:
                    //Affichage d'un pop-up indiquant que la trame n'est pas connu par le uC
                    //Display the MessageBox
                    //string message2 = "Trame inconnue pour le uC";
                    //result = MessageBox.Show(message2, caption, buttons);
                    break;
                default:
                    Console.WriteLine("Default Case");
                    break;
            }
        }
    }

    class Program{
        public static void Main()
        {
            //Console.WriteLine("je suis la ");
            XBeeThread xbee1 = new XBeeThread();
            tTrame trame1 = new tTrame();
            xbee1.Envoyer(trame1.serialize());
            while(true) 
            {
                trame1.deserialize(xbee1.Recevoir());
                //Console.WriteLine(xbee1.Recevoir());
            }
        }
    }
}
