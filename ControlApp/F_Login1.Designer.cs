﻿namespace ControlApp
{
    partial class F_Login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(F_Login));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.TB_User = new System.Windows.Forms.TextBox();
            this.TB_Password = new System.Windows.Forms.TextBox();
            this.BP_Login = new System.Windows.Forms.Button();
            this.BP_Annul = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(61, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "User";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(37, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Password";
            // 
            // TB_User
            // 
            this.TB_User.Location = new System.Drawing.Point(114, 25);
            this.TB_User.Name = "TB_User";
            this.TB_User.Size = new System.Drawing.Size(100, 20);
            this.TB_User.TabIndex = 1;
            // 
            // TB_Password
            // 
            this.TB_Password.Location = new System.Drawing.Point(114, 51);
            this.TB_Password.Name = "TB_Password";
            this.TB_Password.PasswordChar = '*';
            this.TB_Password.Size = new System.Drawing.Size(100, 20);
            this.TB_Password.TabIndex = 2;
            // 
            // BP_Login
            // 
            this.BP_Login.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BP_Login.Location = new System.Drawing.Point(177, 106);
            this.BP_Login.Name = "BP_Login";
            this.BP_Login.Size = new System.Drawing.Size(67, 62);
            this.BP_Login.TabIndex = 3;
            this.BP_Login.Text = "Go";
            this.BP_Login.UseVisualStyleBackColor = true;
            this.BP_Login.Click += new System.EventHandler(this.BP_Login_Click);
            // 
            // BP_Annul
            // 
            this.BP_Annul.BackColor = System.Drawing.Color.DarkGray;
            this.BP_Annul.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.BP_Annul.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BP_Annul.Location = new System.Drawing.Point(40, 106);
            this.BP_Annul.Name = "BP_Annul";
            this.BP_Annul.Size = new System.Drawing.Size(67, 62);
            this.BP_Annul.TabIndex = 4;
            this.BP_Annul.Text = "Annuler";
            this.BP_Annul.UseVisualStyleBackColor = false;
            this.BP_Annul.Click += new System.EventHandler(this.BP_Annul_Click);
            // 
            // F_Login
            // 
            this.AcceptButton = this.BP_Login;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.CancelButton = this.BP_Annul;
            this.ClientSize = new System.Drawing.Size(286, 186);
            this.Controls.Add(this.BP_Annul);
            this.Controls.Add(this.BP_Login);
            this.Controls.Add(this.TB_Password);
            this.Controls.Add(this.TB_User);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "F_Login";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Login";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.F_Login_FormClosing);
            this.Load += new System.EventHandler(this.F_Login_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TB_User;
        private System.Windows.Forms.TextBox TB_Password;
        private System.Windows.Forms.Button BP_Login;
        private System.Windows.Forms.Button BP_Annul;
    }
}