﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;
using System.Windows.Forms;

namespace ControlApp
{
    /// <summary>
    /// Fenetre de debug à la main, permet de tester l'envoi de valeurs 
    /// Toff: Temps avant de commuter la PWM a 0 (image du rapport cyclique)
    /// FreqPCA: Fréquence de la PWM
    /// </summary>
    public partial class FenetreDebug : Form
    {
        F_Principale papa;
        string dataIn;

        public FenetreDebug()
        {
            InitializeComponent();
        }

        private void BP_debug0_Click(object sender, EventArgs e)
        {
            papa.comXbee.Envoyer("0");
        }

        private void FenetreDebug_Load(object sender, EventArgs e)
        {
           papa = (F_Principale)this.Owner;
           TextToff.Text = sliderToff.Value.ToString();
           textFreqPCA.Text = sliderFreqPCA.Value.ToString();
           papa.comXbee.Envoyer("11");
        }

        private void BP_debug6_Click(object sender, EventArgs e)
        {
            papa.comXbee.EmptyBuffer();
            papa.comXbee.Envoyer("6");
            TextDebug.Text  = papa.comXbee.RecevoirInt();
            TextDebug.Refresh();
        }

        private void BP_debug1_Click(object sender, EventArgs e)
        {
            papa.comXbee.Envoyer("1");
        }

        private void BP_debug2_Click(object sender, EventArgs e)
        {
            papa.comXbee.Envoyer("2");
        }

        private void BP_debug3_Click(object sender, EventArgs e)
        {
            papa.comXbee.Envoyer("3");
        }


        private void BP_debugSetToff_Click(object sender, EventArgs e)
        {
            papa.comXbee.Envoyer("3");
            papa.comXbee.Envoyer(sliderToff.Value.ToString());
        }

        private void sliderToff_ValueChanged(object sender, EventArgs e)
        {
            TextToff.Text = sliderToff.Value.ToString();
        }

        private void sliderFreqPCA_ValueChanged(object sender, EventArgs e)
        {
            textFreqPCA.Text = sliderFreqPCA.Value.ToString();
        }

        private void BP_debugSetFreqPCA_Click(object sender, EventArgs e)
        {
            papa.comXbee.Envoyer("7");
            float val = ((25000000 /( 4096 * sliderFreqPCA.Value)) - 1 )  ;
            papa.comXbee.Envoyer(Math.Round(val,0).ToString());
        }

        private void FenetreDebug_FormClosing(object sender, FormClosingEventArgs e)
        {
            papa.comXbee.Envoyer("10");
        }

        private void BP_Clignot_Click(object sender, EventArgs e)
        {
            papa.comXbee.Envoyer("8");
        }
    }
}
