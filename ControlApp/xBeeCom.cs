﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.IO.Ports;

namespace ControlApp
{
    /// <summary>
    /// Classe de gestion du port COM de l'XBEE : Ok, utilisée
    /// </summary>
    public class XBeeCom
    {
        static bool _continue;
        SerialPort myXbee;
        
        public XBeeCom(string port)
        {
                myXbee = new SerialPort(port);
                myXbee.BaudRate = 9600;
                myXbee.Parity = Parity.None;
                myXbee.StopBits = StopBits.One;
                myXbee.DataBits = 8;
                myXbee.WriteTimeout = 500;
                myXbee.ReadTimeout = 1000;
                myXbee.NewLine = "\n";
                if (!myXbee.IsOpen) myXbee.Open();
           
        }

        public bool IsConnected()
        {
            if (!myXbee.IsOpen) myXbee.Open();
            myXbee.Write("10\n"); // test connection
            try {
                Console.WriteLine(myXbee.ReadLine());
            }

            catch 
            {
                System.Windows.Forms.MessageBox.Show("Pas de réponse...");
                return false;
            }

            return true;
            }
    

        public void Envoyer(string data)
        {
            myXbee.WriteLine(data);
        }

        public string Recevoir()
        {
            return myXbee.ReadLine();
        }

        public string RecevoirInt()
        {
            string val="";
            int tampon = 0;
            while (tampon !=10)
            {
                tampon = myXbee.ReadByte();
                val +=  tampon.ToString();
                val += " ";
            }
            return (val);
        }

        public bool IsOpen()
        {
            return myXbee.IsOpen;
        }
        public void Close()
        {
            myXbee.Close();
        }

        public void EmptyBuffer()
        {
            while (myXbee.BytesToRead > 0)
                Recevoir();
        }

    }

    /// <summary>
    /// Classe de gestion des trames: A revoir, non utilisée
    /// </summary>
    class Trame
    {
        int idTrame = 10;
        String data;

        public String serialize()
        {
            String trameToSend = "";
            switch (idTrame)
            {
                case 1:
                    //Afficher le motif(id motif)
                    trameToSend = "1;" + data;
                    break;
                case 2:
                    //Afficher le pg lumineux(id pg)
                    trameToSend = "1;" + data;
                    break;
                case 3:
                    //Stopper l'exécution du programme en cours
                    trameToSend = "3";
                    break;
                case 4:
                    //Stocker programme dans la mémoire non-volatile du uC
                    trameToSend = "4;" + data;
                    break;
                case 6:
                    //Quel est le programme en cours d'exécution ?
                    trameToSend = "6";
                    break;
                case 8:
                    //Quelle est la luminosité mesurée ?
                    trameToSend = "8";
                    break;
                case 10:
                    //Test de communication
                    trameToSend = "10";
                    break;
                default:
                    Console.WriteLine("Default Case");
                    break;
            }
            trameToSend = trameToSend + ";Fin";
            Console.WriteLine("Trame envoyee : {0}", trameToSend);

            return trameToSend;
        }

        public void deserialize(string trameToRead)
        {
            String paramsRetour = "";
            int idRetour;
            char[] delimeterChars = { ';' };
            string[] words = trameToRead.Split(delimeterChars);
            idRetour = Int32.Parse(words[0]);
            //paramsRetour = words[1];
            /*MessageBoxButtons buttons = MessageBoxButtons.OK;
            DialogResult result;
            const string caption = "Form Closing";*/
            Console.WriteLine("Trame reçue : {0}Fin", trameToRead);

            switch (idRetour)
            {
                case 7:
                    paramsRetour = words[1];
                    Console.WriteLine("Id de la trame reçue : {0}", idRetour);
                    Console.WriteLine("Contenu de la trame reçue : {0}",paramsRetour);
                    //Recherche du nom du programme en cours d'exécution par rapport à l'id retourné par le uC
                    //Display the MessageBox
                    //result = MessageBox.Show(paramsRetour, caption, buttons);
                    break;
                case 9:
                    paramsRetour = words[1];
                    //Affichage d'un pop-up qui affiche la valeur la valeur contenue dans paramsRetour
                    //Display the MessageBox
                    //result = MessageBox.Show(paramsRetour, caption, buttons);
                    break;
                case 11:
                    //Affichage d'un pop-up indiquant que le test de communication avec le système distant est concluant
                    //Display the MessageBox
                    //string message = "Test communication OK";
                    //result = MessageBox.Show(message, caption, buttons);
                    break;
                case 12:
                    //Affichage d'un pop-up indiquant que la trame n'est pas connu par le uC
                    //Display the MessageBox
                    //string message2 = "Trame inconnue pour le uC";
                    //result = MessageBox.Show(message2, caption, buttons);
                    break;
                default:
                    Console.WriteLine("Default Case");
                    break;
            }
        }
    }
}
