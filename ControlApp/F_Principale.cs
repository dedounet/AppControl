﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO.Ports;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace ControlApp
{
    /// <summary>
    /// Fenètre principale de l'application, elle appelle et gère l'ensemble des fonctionnalités
    /// </summary>
    public partial class F_Principale : Form
    {
        public XBeeCom comXbee; //objet Xbee lié au port com
        string[] ports = SerialPort.GetPortNames(); 
        private bool connexion =false;  // image de l'état de la connection au système
        private bool admin= false ;
        public F_Principale()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Permet la communication avec l'interface de login
        /// </summary>
        /// <param name="loginResult"></param>
        public void LoginState(string loginResult)
        {
            if (loginResult == "OK Admin")
            {
                this.WindowState = FormWindowState.Normal;

            }
            else if(loginResult == "OK Normal")
            {
                this.WindowState = FormWindowState.Normal;
                BP_GestMotif.Hide();
            }
            else
            {
                this.Close();
                this.Dispose();
            }
        }

        /// <summary>
        /// Chargement de la fenetre principale : non affichée au démarage
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void F_Principale_Load(object sender, EventArgs e)
        {
            using (F_Login fenetre_login = new F_Login())
            {
                fenetre_login.ShowDialog(this);
            }

            RefreshPortComList();

            MotifBox.Enabled = false;
            SceneBox.Enabled = false;

        }

        /// <summary>
        /// Au clic sur le bouton connexion, on test le retour d'informations du microcontrolleur, si valide, 
        /// on active les controles et on change le boutton en "Déconnexion"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BP_Connexion_Click(object sender, EventArgs e)
        {
            if (connexion == false)
            {
                if (comXbee ==null)
                    comXbee = new XBeeCom(listPortCom.SelectedItem.ToString());
                if (comXbee.IsConnected())
                {
                    MotifBox.Enabled = true;
                    SceneBox.Enabled = true;
                    LabelConnect.Text = "Connecté";
                    connexion = true;
                    BP_Connexion.Text = "Déconnecter";
                }
            }
            else {
                MotifBox.Enabled = false;
                SceneBox.Enabled = false;
                comXbee.Close();
                BP_Connexion.Text = "Connecter";
                connexion = false;
            }
        }

        private void RefreshPortCom_Click(object sender, EventArgs e)
        {
            RefreshPortComList();
        }

        private void RefreshPortComList()
        {
            listPortCom.Items.Clear();
            ports = SerialPort.GetPortNames();
            foreach (string port in ports)
            {
                listPortCom.Items.Add(port);
                listPortCom.Refresh();
            }
            listPortCom.SelectedIndex=  listPortCom.FindString("COM");
        }

        private void BP_NewMotif_Click(object sender, EventArgs e)
        {
        
            if ( motif.IHMCreerMotif.Instance == null)
            {
                motif.IHMCreerMotif.Instance.BringToFront();
            }
            else
            {
                motif.IHMCreerMotif fenetre_motif = motif.IHMCreerMotif.Instance;
                fenetre_motif.Show();
            }
            
        }

        private void F_Principale_FormClosed(object sender, FormClosedEventArgs e)
        {
           if  (comXbee!= null) comXbee.Close();
            Dispose();

        }

        private void BP_ShowMotif_Click(object sender, EventArgs e)
        {

            if (motif.FormAfficherMotif.Instance == null)
            {
                motif.FormAfficherMotif.Instance.BringToFront();
            }
            else
            {
                motif.FormAfficherMotif fenetre_afficher = motif.FormAfficherMotif.Instance;
                fenetre_afficher.Show();
            }
        }



        private void LabelConnect_Click(object sender, EventArgs e)
        {
            using (FenetreDebug fenetre_debug = new FenetreDebug())
            {
                if( connexion == true)
                fenetre_debug.ShowDialog(this);
            }
        }
        /// <summary>
        /// au clic on envoi les objets selectionnés dans la liste de motif dans la liste de droite pour constituer le programme
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pictureFleche_Click(object sender, EventArgs e)
        {
            
        }

    }

}
