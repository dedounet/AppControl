﻿namespace ControlApp
{
    partial class FenetreDebug
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FenetreDebug));
            this.BP_debug0 = new System.Windows.Forms.Button();
            this.BP_debug1 = new System.Windows.Forms.Button();
            this.BP_debug2 = new System.Windows.Forms.Button();
            this.BP_debug6 = new System.Windows.Forms.Button();
            this.sliderToff = new System.Windows.Forms.TrackBar();
            this.label1 = new System.Windows.Forms.Label();
            this.BP_debug3 = new System.Windows.Forms.Button();
            this.TextDebug = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.BP_debugSetToff = new System.Windows.Forms.Button();
            this.TextToff = new System.Windows.Forms.Label();
            this.sliderFreqPCA = new System.Windows.Forms.TrackBar();
            this.BP_debugSetFreqPCA = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.textFreqPCA = new System.Windows.Forms.Label();
            this.BP_Clignot = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.sliderToff)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sliderFreqPCA)).BeginInit();
            this.SuspendLayout();
            // 
            // BP_debug0
            // 
            this.BP_debug0.Location = new System.Drawing.Point(32, 21);
            this.BP_debug0.Name = "BP_debug0";
            this.BP_debug0.Size = new System.Drawing.Size(67, 36);
            this.BP_debug0.TabIndex = 0;
            this.BP_debug0.Text = "Mode 0 ";
            this.BP_debug0.UseVisualStyleBackColor = true;
            this.BP_debug0.Click += new System.EventHandler(this.BP_debug0_Click);
            // 
            // BP_debug1
            // 
            this.BP_debug1.Location = new System.Drawing.Point(32, 63);
            this.BP_debug1.Name = "BP_debug1";
            this.BP_debug1.Size = new System.Drawing.Size(67, 36);
            this.BP_debug1.TabIndex = 1;
            this.BP_debug1.Text = "Mode1 Toff+100";
            this.BP_debug1.UseVisualStyleBackColor = true;
            this.BP_debug1.Click += new System.EventHandler(this.BP_debug1_Click);
            // 
            // BP_debug2
            // 
            this.BP_debug2.Location = new System.Drawing.Point(32, 105);
            this.BP_debug2.Name = "BP_debug2";
            this.BP_debug2.Size = new System.Drawing.Size(67, 36);
            this.BP_debug2.TabIndex = 2;
            this.BP_debug2.Text = "Mode2 Toff-100";
            this.BP_debug2.UseVisualStyleBackColor = true;
            this.BP_debug2.Click += new System.EventHandler(this.BP_debug2_Click);
            // 
            // BP_debug6
            // 
            this.BP_debug6.Location = new System.Drawing.Point(32, 189);
            this.BP_debug6.Name = "BP_debug6";
            this.BP_debug6.Size = new System.Drawing.Size(67, 36);
            this.BP_debug6.TabIndex = 3;
            this.BP_debug6.Text = "Mode6 lecture tab";
            this.BP_debug6.UseVisualStyleBackColor = true;
            this.BP_debug6.Click += new System.EventHandler(this.BP_debug6_Click);
            // 
            // sliderToff
            // 
            this.sliderToff.Location = new System.Drawing.Point(115, 21);
            this.sliderToff.Maximum = 4090;
            this.sliderToff.Minimum = 11;
            this.sliderToff.Name = "sliderToff";
            this.sliderToff.Size = new System.Drawing.Size(128, 45);
            this.sliderToff.TabIndex = 4;
            this.sliderToff.Value = 11;
            this.sliderToff.ValueChanged += new System.EventHandler(this.sliderToff_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(151, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Toff :";
            // 
            // BP_debug3
            // 
            this.BP_debug3.Location = new System.Drawing.Point(32, 147);
            this.BP_debug3.Name = "BP_debug3";
            this.BP_debug3.Size = new System.Drawing.Size(67, 36);
            this.BP_debug3.TabIndex = 3;
            this.BP_debug3.Text = "Mode3 Toff=1000";
            this.BP_debug3.UseVisualStyleBackColor = true;
            this.BP_debug3.Click += new System.EventHandler(this.BP_debug3_Click);
            // 
            // TextDebug
            // 
            this.TextDebug.AutoSize = true;
            this.TextDebug.Location = new System.Drawing.Point(15, 16);
            this.TextDebug.Name = "TextDebug";
            this.TextDebug.Size = new System.Drawing.Size(0, 13);
            this.TextDebug.TabIndex = 6;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.TextDebug);
            this.groupBox1.Location = new System.Drawing.Point(115, 147);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(262, 78);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "MsgReçu";
            // 
            // BP_debugSetToff
            // 
            this.BP_debugSetToff.Location = new System.Drawing.Point(130, 54);
            this.BP_debugSetToff.Name = "BP_debugSetToff";
            this.BP_debugSetToff.Size = new System.Drawing.Size(101, 36);
            this.BP_debugSetToff.TabIndex = 3;
            this.BP_debugSetToff.Text = "Set Toff";
            this.BP_debugSetToff.UseVisualStyleBackColor = true;
            this.BP_debugSetToff.Click += new System.EventHandler(this.BP_debugSetToff_Click);
            // 
            // TextToff
            // 
            this.TextToff.AutoSize = true;
            this.TextToff.Location = new System.Drawing.Point(199, 9);
            this.TextToff.Name = "TextToff";
            this.TextToff.Size = new System.Drawing.Size(0, 13);
            this.TextToff.TabIndex = 8;
            // 
            // sliderFreqPCA
            // 
            this.sliderFreqPCA.Location = new System.Drawing.Point(249, 21);
            this.sliderFreqPCA.Maximum = 1526;
            this.sliderFreqPCA.Minimum = 24;
            this.sliderFreqPCA.Name = "sliderFreqPCA";
            this.sliderFreqPCA.Size = new System.Drawing.Size(128, 45);
            this.sliderFreqPCA.TabIndex = 4;
            this.sliderFreqPCA.Value = 60;
            this.sliderFreqPCA.ValueChanged += new System.EventHandler(this.sliderFreqPCA_ValueChanged);
            // 
            // BP_debugSetFreqPCA
            // 
            this.BP_debugSetFreqPCA.Location = new System.Drawing.Point(264, 54);
            this.BP_debugSetFreqPCA.Name = "BP_debugSetFreqPCA";
            this.BP_debugSetFreqPCA.Size = new System.Drawing.Size(101, 36);
            this.BP_debugSetFreqPCA.TabIndex = 3;
            this.BP_debugSetFreqPCA.Text = "Set FreqPCA";
            this.BP_debugSetFreqPCA.UseVisualStyleBackColor = true;
            this.BP_debugSetFreqPCA.Click += new System.EventHandler(this.BP_debugSetFreqPCA_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(277, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "FreqPCA :";
            // 
            // textFreqPCA
            // 
            this.textFreqPCA.AutoSize = true;
            this.textFreqPCA.Location = new System.Drawing.Point(325, 9);
            this.textFreqPCA.Name = "textFreqPCA";
            this.textFreqPCA.Size = new System.Drawing.Size(0, 13);
            this.textFreqPCA.TabIndex = 8;
            // 
            // BP_Clignot
            // 
            this.BP_Clignot.BackColor = System.Drawing.SystemColors.Info;
            this.BP_Clignot.Location = new System.Drawing.Point(223, 101);
            this.BP_Clignot.Name = "BP_Clignot";
            this.BP_Clignot.Size = new System.Drawing.Size(48, 40);
            this.BP_Clignot.TabIndex = 9;
            this.BP_Clignot.Text = "Clignot";
            this.BP_Clignot.UseMnemonic = false;
            this.BP_Clignot.UseVisualStyleBackColor = false;
            this.BP_Clignot.Click += new System.EventHandler(this.BP_Clignot_Click);
            // 
            // FenetreDebug
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(407, 255);
            this.Controls.Add(this.BP_Clignot);
            this.Controls.Add(this.textFreqPCA);
            this.Controls.Add(this.TextToff);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.BP_debug6);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.BP_debugSetFreqPCA);
            this.Controls.Add(this.BP_debugSetToff);
            this.Controls.Add(this.BP_debug3);
            this.Controls.Add(this.BP_debug2);
            this.Controls.Add(this.BP_debug1);
            this.Controls.Add(this.BP_debug0);
            this.Controls.Add(this.sliderFreqPCA);
            this.Controls.Add(this.sliderToff);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FenetreDebug";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "FenetreDebug";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FenetreDebug_FormClosing);
            this.Load += new System.EventHandler(this.FenetreDebug_Load);
            ((System.ComponentModel.ISupportInitialize)(this.sliderToff)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sliderFreqPCA)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BP_debug0;
        private System.Windows.Forms.Button BP_debug1;
        private System.Windows.Forms.Button BP_debug2;
        private System.Windows.Forms.Button BP_debug6;
        private System.Windows.Forms.TrackBar sliderToff;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button BP_debug3;
        private System.Windows.Forms.Label TextDebug;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button BP_debugSetToff;
        private System.Windows.Forms.Label TextToff;
        private System.Windows.Forms.TrackBar sliderFreqPCA;
        private System.Windows.Forms.Button BP_debugSetFreqPCA;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label textFreqPCA;
        private System.Windows.Forms.Button BP_Clignot;
    }
}