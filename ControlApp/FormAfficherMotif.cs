﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace motif
{
    /// <summary>
    /// Fenêtre utilisée pour sélectionner un des motifs stockés dans le système déporté et l'afficher
    /// </summary>
    public partial class FormAfficherMotif : Form
    {
        static FormAfficherMotif n_instance;

        public static FormAfficherMotif Instance
        {
            get
            {
                return n_instance ?? (n_instance = new FormAfficherMotif());
            }
        }

        public void resetInstance() => n_instance = null;

        public FormAfficherMotif()
        {
            InitializeComponent();
        }

        private void checkedListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void listBoxMotif_SelectedIndexChanged(object sender, EventArgs e)
        {
            //exemple de remplissage
            /*listBoxMotif.BeginUpdate();
            listBoxMotif.Items.Add("Essais");
            listBoxMotif.Items.Add("c'est compris");
            listBoxMotif.EndUpdate();*/
        }

        private void BpSelectionner_Click(object sender, EventArgs e)
        {
           
        }

        private void FormAfficherMotif_FormClosed(object sender, FormClosedEventArgs e)
        {
            FormAfficherMotif.Instance.resetInstance();
        }
    }
}
