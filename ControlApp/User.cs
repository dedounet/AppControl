﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControlApp
{
    public class User : IEquatable<User>
    {
        private string userName { get; set; }
        private string password { get; set; }


    User()
        {

        }

    public  User(string userName, string password)
        {
            this.userName = userName;
            this.password = password;

        }
        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            User objAsUser= obj as User;
            if (objAsUser == null) return false;
            else return Equals(objAsUser);
        }

        public bool Equals(User other)
        {
            if (other == null) return false;
            return (this.userName.Equals(other.userName)&&this.password.Equals(other.password));
        }

        public override int GetHashCode()
        {
            var hashCode = -514035047;
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(userName);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(password);
            return hashCode;
        }
    }
}
