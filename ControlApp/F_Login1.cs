﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ControlApp
{
    /// <summary>
    /// Fenetre de login : utilisée et testée
    /// </summary>
    public partial class F_Login : Form
    {
        //attributs
        List<User> userList = new List<User>();
        //Création des 2 Users de base
        User userNormal = new User("user", "password");
        User userAdmin = new User("admin", "admin");

        private bool logIsDone = false; 
       
       
        public F_Login()
        {
            InitializeComponent();
        }

        public void LoginResult(string reponse) {
            F_Principale parent = (F_Principale)this.Owner;
            parent.LoginState(reponse);
         }


        private void BP_Login_Click(object sender, EventArgs e)
        {
            TestUser();
        }

        private void F_Login_Load(object sender, EventArgs e)
        {
            userList.Add(userNormal);
            userList.Add(userAdmin);
            
        }

        private void BP_Annul_Click(object sender, EventArgs e)
        {
            LoginResult("NOK");
        }

        private void TestUser()
        {
            if (!string.IsNullOrEmpty(TB_User.Text) && !string.IsNullOrEmpty(TB_Password.Text))
            {
                try
                {
                    User login = new User(TB_User.Text, TB_Password.Text);
                    //Console.WriteLine(userList.Contains(new User(TB_User.Text, TB_Password.Text)));

                    if (login.Equals(userAdmin))
                    {
                        LoginResult("OK Admin");
                        logIsDone = true;
                        this.Close();

                    }else if(login.Equals(userNormal))
                    {
                        LoginResult("OK Normal");
                        logIsDone = true;
                        this.Close();
                    }
                    else MessageBox.Show("Login incorrect");
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }

            }
            else
            {
                MessageBox.Show("Login incorrect");
            }
        }

        private void F_Login_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!logIsDone)
                Application.Exit(); 
            
        }
    }
}
