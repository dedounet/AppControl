﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace motif
{
    /// <summary>
    /// Fenetre de création de motif , modifs a faire: dessiner les segments du panneau et les rendre clickable
    /// </summary>
    public partial class IHMCreerMotif : Form
    {
        static IHMCreerMotif n_instance;
        /*-----images a charger pour montrer la selection de la bande---*/
        //private bandeVertSel = Image.FromFile("BandeVert.png");
        //private bandeVertUnsel = Image.FromFile("BandeVertClair.png");


        public static IHMCreerMotif Instance
        {
            get {
                return n_instance ?? (n_instance = new IHMCreerMotif());
            }
        }

        public void resetInstance() => n_instance = null;

        public IHMCreerMotif()
        {
            InitializeComponent();
        }

        /* Fonction save_Click()
         * Actions faites lors du clique sur le bouton "Save" de l'IHM" */ 
        private void save_Click(object sender, EventArgs e)
        {
            String valueName = textBoxName.Text;
            int valueTime = Convert.ToInt16(textBoxTime.Text);


            //tMotif.SetName(valueName);

        }

        private void BPCancelMotif_Click(object sender, EventArgs e)
        {
            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            DialogResult result;
            const string caption = "Création du motif annulée";
            string message = "Vous êtes sur le point d'annuler la création du motif. Voulez-vous poursuivre ?";
            //Display the MessageBox
            result = MessageBox.Show(message, caption, buttons);
            if (result == System.Windows.Forms.DialogResult.Yes)
                this.Close();
        }

        private void IHMCreerMotif_FormClosed(object sender, FormClosedEventArgs e)
        {
            IHMCreerMotif.Instance.resetInstance();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }
    }
}
