﻿namespace ControlApp
{
    partial class F_Principale
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(F_Principale));
            this.BP_Connexion = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.RefreshPortCom = new System.Windows.Forms.PictureBox();
            this.LabelConnect = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.listPortCom = new System.Windows.Forms.ComboBox();
            this.PanelCom = new System.Windows.Forms.Panel();
            this.BP_GestMotif = new System.Windows.Forms.Button();
            this.BP_ShowMotif = new System.Windows.Forms.Button();
            this.MotifBox = new System.Windows.Forms.GroupBox();
            this.list_SceneIn = new System.Windows.Forms.ListBox();
            this.list_SceneOut = new System.Windows.Forms.ListBox();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.SceneBox = new System.Windows.Forms.GroupBox();
            this.BP_EnvoyerScene = new System.Windows.Forms.Button();
            this.PicFlecheOut = new System.Windows.Forms.PictureBox();
            this.PicFlecheIn = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.labelSceneIn = new System.Windows.Forms.Label();
            this.labelSceneOut = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.RefreshPortCom)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.PanelCom.SuspendLayout();
            this.MotifBox.SuspendLayout();
            this.SceneBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PicFlecheOut)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicFlecheIn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // BP_Connexion
            // 
            this.BP_Connexion.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BP_Connexion.ForeColor = System.Drawing.SystemColors.ControlText;
            this.BP_Connexion.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BP_Connexion.Location = new System.Drawing.Point(16, 54);
            this.BP_Connexion.Name = "BP_Connexion";
            this.BP_Connexion.Size = new System.Drawing.Size(74, 26);
            this.BP_Connexion.TabIndex = 2;
            this.BP_Connexion.Text = "Connexion";
            this.BP_Connexion.UseVisualStyleBackColor = true;
            this.BP_Connexion.Click += new System.EventHandler(this.BP_Connexion_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Port Com";
            // 
            // RefreshPortCom
            // 
            this.RefreshPortCom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.RefreshPortCom.Image = global::ControlApp.Properties.Resources.Refresh;
            this.RefreshPortCom.InitialImage = global::ControlApp.Properties.Resources.Refresh;
            this.RefreshPortCom.Location = new System.Drawing.Point(116, 16);
            this.RefreshPortCom.Name = "RefreshPortCom";
            this.RefreshPortCom.Size = new System.Drawing.Size(24, 27);
            this.RefreshPortCom.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.RefreshPortCom.TabIndex = 5;
            this.RefreshPortCom.TabStop = false;
            this.RefreshPortCom.Click += new System.EventHandler(this.RefreshPortCom_Click);
            // 
            // LabelConnect
            // 
            this.LabelConnect.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.LabelConnect.Name = "LabelConnect";
            this.LabelConnect.Size = new System.Drawing.Size(70, 17);
            this.LabelConnect.Text = "Déconnecté";
            this.LabelConnect.Click += new System.EventHandler(this.LabelConnect_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.LabelConnect});
            this.statusStrip1.Location = new System.Drawing.Point(0, 283);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.ManagerRenderMode;
            this.statusStrip1.Size = new System.Drawing.Size(579, 22);
            this.statusStrip1.TabIndex = 8;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // listPortCom
            // 
            this.listPortCom.FormattingEnabled = true;
            this.listPortCom.Location = new System.Drawing.Point(6, 16);
            this.listPortCom.Name = "listPortCom";
            this.listPortCom.Size = new System.Drawing.Size(104, 21);
            this.listPortCom.TabIndex = 9;
            // 
            // PanelCom
            // 
            this.PanelCom.Controls.Add(this.BP_Connexion);
            this.PanelCom.Controls.Add(this.label1);
            this.PanelCom.Controls.Add(this.listPortCom);
            this.PanelCom.Controls.Add(this.RefreshPortCom);
            this.PanelCom.Location = new System.Drawing.Point(12, 12);
            this.PanelCom.Name = "PanelCom";
            this.PanelCom.Size = new System.Drawing.Size(151, 94);
            this.PanelCom.TabIndex = 11;
            // 
            // BP_GestMotif
            // 
            this.BP_GestMotif.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BP_GestMotif.Location = new System.Drawing.Point(15, 19);
            this.BP_GestMotif.Name = "BP_GestMotif";
            this.BP_GestMotif.Size = new System.Drawing.Size(82, 40);
            this.BP_GestMotif.TabIndex = 6;
            this.BP_GestMotif.Text = "Gestion motif";
            this.BP_GestMotif.UseVisualStyleBackColor = true;
            this.BP_GestMotif.Click += new System.EventHandler(this.BP_NewMotif_Click);
            // 
            // BP_ShowMotif
            // 
            this.BP_ShowMotif.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BP_ShowMotif.Location = new System.Drawing.Point(15, 71);
            this.BP_ShowMotif.Name = "BP_ShowMotif";
            this.BP_ShowMotif.Size = new System.Drawing.Size(82, 40);
            this.BP_ShowMotif.TabIndex = 7;
            this.BP_ShowMotif.Text = "Afficher motif";
            this.BP_ShowMotif.UseVisualStyleBackColor = true;
            this.BP_ShowMotif.Click += new System.EventHandler(this.BP_ShowMotif_Click);
            // 
            // MotifBox
            // 
            this.MotifBox.Controls.Add(this.BP_ShowMotif);
            this.MotifBox.Controls.Add(this.BP_GestMotif);
            this.MotifBox.Location = new System.Drawing.Point(28, 129);
            this.MotifBox.Name = "MotifBox";
            this.MotifBox.Size = new System.Drawing.Size(113, 117);
            this.MotifBox.TabIndex = 10;
            this.MotifBox.TabStop = false;
            this.MotifBox.Text = "Gestion de motif";
            // 
            // list_SceneIn
            // 
            this.list_SceneIn.FormattingEnabled = true;
            this.list_SceneIn.Location = new System.Drawing.Point(7, 36);
            this.list_SceneIn.Name = "list_SceneIn";
            this.list_SceneIn.Size = new System.Drawing.Size(120, 134);
            this.list_SceneIn.TabIndex = 12;
            // 
            // list_SceneOut
            // 
            this.list_SceneOut.FormattingEnabled = true;
            this.list_SceneOut.Location = new System.Drawing.Point(209, 36);
            this.list_SceneOut.Name = "list_SceneOut";
            this.list_SceneOut.Size = new System.Drawing.Size(120, 134);
            this.list_SceneOut.TabIndex = 13;
            // 
            // SceneBox
            // 
            this.SceneBox.Controls.Add(this.labelSceneOut);
            this.SceneBox.Controls.Add(this.labelSceneIn);
            this.SceneBox.Controls.Add(this.BP_EnvoyerScene);
            this.SceneBox.Controls.Add(this.PicFlecheOut);
            this.SceneBox.Controls.Add(this.PicFlecheIn);
            this.SceneBox.Controls.Add(this.list_SceneOut);
            this.SceneBox.Controls.Add(this.pictureBox1);
            this.SceneBox.Controls.Add(this.list_SceneIn);
            this.SceneBox.Location = new System.Drawing.Point(210, 28);
            this.SceneBox.Name = "SceneBox";
            this.SceneBox.Size = new System.Drawing.Size(337, 201);
            this.SceneBox.TabIndex = 15;
            this.SceneBox.TabStop = false;
            this.SceneBox.Text = "Gestion de scene";
            // 
            // BP_EnvoyerScene
            // 
            this.BP_EnvoyerScene.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BP_EnvoyerScene.Location = new System.Drawing.Point(229, 171);
            this.BP_EnvoyerScene.Name = "BP_EnvoyerScene";
            this.BP_EnvoyerScene.Size = new System.Drawing.Size(80, 23);
            this.BP_EnvoyerScene.TabIndex = 15;
            this.BP_EnvoyerScene.Text = "Envoyer";
            this.BP_EnvoyerScene.UseVisualStyleBackColor = true;
            // 
            // PicFlecheOut
            // 
            this.PicFlecheOut.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.PicFlecheOut.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicFlecheOut.Image = ((System.Drawing.Image)(resources.GetObject("PicFlecheOut.Image")));
            this.PicFlecheOut.InitialImage = ((System.Drawing.Image)(resources.GetObject("PicFlecheOut.InitialImage")));
            this.PicFlecheOut.Location = new System.Drawing.Point(151, 104);
            this.PicFlecheOut.Name = "PicFlecheOut";
            this.PicFlecheOut.Size = new System.Drawing.Size(35, 35);
            this.PicFlecheOut.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicFlecheOut.TabIndex = 14;
            this.PicFlecheOut.TabStop = false;
            this.PicFlecheOut.Click += new System.EventHandler(this.pictureFleche_Click);
            // 
            // PicFlecheIn
            // 
            this.PicFlecheIn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.PicFlecheIn.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicFlecheIn.Image = ((System.Drawing.Image)(resources.GetObject("PicFlecheIn.Image")));
            this.PicFlecheIn.InitialImage = ((System.Drawing.Image)(resources.GetObject("PicFlecheIn.InitialImage")));
            this.PicFlecheIn.Location = new System.Drawing.Point(151, 57);
            this.PicFlecheIn.Name = "PicFlecheIn";
            this.PicFlecheIn.Size = new System.Drawing.Size(35, 35);
            this.PicFlecheIn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicFlecheIn.TabIndex = 14;
            this.PicFlecheIn.TabStop = false;
            this.PicFlecheIn.Click += new System.EventHandler(this.pictureFleche_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Image = global::ControlApp.Properties.Resources.Refresh;
            this.pictureBox1.InitialImage = global::ControlApp.Properties.Resources.Refresh;
            this.pictureBox1.Location = new System.Drawing.Point(102, 143);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(24, 27);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.RefreshPortCom_Click);
            // 
            // labelSceneIn
            // 
            this.labelSceneIn.AutoSize = true;
            this.labelSceneIn.Location = new System.Drawing.Point(6, 20);
            this.labelSceneIn.Name = "labelSceneIn";
            this.labelSceneIn.Size = new System.Drawing.Size(90, 13);
            this.labelSceneIn.TabIndex = 16;
            this.labelSceneIn.Text = "Motifs disponibles";
            // 
            // labelSceneOut
            // 
            this.labelSceneOut.AutoSize = true;
            this.labelSceneOut.Location = new System.Drawing.Point(206, 20);
            this.labelSceneOut.Name = "labelSceneOut";
            this.labelSceneOut.Size = new System.Drawing.Size(91, 13);
            this.labelSceneOut.TabIndex = 16;
            this.labelSceneOut.Text = "Scenes à charger";
            // 
            // F_Principale
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(579, 305);
            this.Controls.Add(this.SceneBox);
            this.Controls.Add(this.PanelCom);
            this.Controls.Add(this.MotifBox);
            this.Controls.Add(this.statusStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "F_Principale";
            this.Text = "IHM Controle";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.F_Principale_FormClosed);
            this.Load += new System.EventHandler(this.F_Principale_Load);
            ((System.ComponentModel.ISupportInitialize)(this.RefreshPortCom)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.PanelCom.ResumeLayout(false);
            this.PanelCom.PerformLayout();
            this.MotifBox.ResumeLayout(false);
            this.SceneBox.ResumeLayout(false);
            this.SceneBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PicFlecheOut)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicFlecheIn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button BP_Connexion;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox RefreshPortCom;
        private System.Windows.Forms.ToolStripStatusLabel LabelConnect;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ComboBox listPortCom;
        private System.Windows.Forms.Panel PanelCom;
        private System.Windows.Forms.Button BP_GestMotif;
        private System.Windows.Forms.Button BP_ShowMotif;
        private System.Windows.Forms.GroupBox MotifBox;
        private System.Windows.Forms.ListBox list_SceneIn;
        private System.Windows.Forms.ListBox list_SceneOut;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.GroupBox SceneBox;
        private System.Windows.Forms.Button BP_EnvoyerScene;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox PicFlecheOut;
        private System.Windows.Forms.PictureBox PicFlecheIn;
        private System.Windows.Forms.Label labelSceneOut;
        private System.Windows.Forms.Label labelSceneIn;
    }
}